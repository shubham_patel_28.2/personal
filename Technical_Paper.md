# Importance of Event Sourcing Architecture

## Abstract
Event sourcing which is related to microservices plays significant role in capturing states of system as sequence of immutable events. In this technical paper I am going to explain core concept and architecture of event sourcing in detail. we will discuss here about adoption of event sourcing, why we need it also, what are the problems it can solve. 

## Introduction

Lets talk about event exactly what it is. Event is the action or state that change within the system. So, every time an action is executing that time a meta data about action will store in the event store and this meta data is immutable i.e. once created will not modify.
So, event sourcing have some characteristics
- **Immutable:** Once data about action will store in the event store then further modification are restricted and it maintains integrity.
- **TimeStamp:** It is very important each action has a timestamp which helps for tracking in the future if any error will happen.
- **Append only:** When an event appended in the event log or event store then neither it will be deleted nor updated in the future. Always we listen about a term  footprints in the internet which is the meta data of event which helps in tracking and also help in other ways also.
- **Descriptive:** Every event is named in such a way that clearly describe the action of event. Example: an event of converting file into pdf format named as pdf convertor make sense by his name similarly event names are created.

### Components and flow in event sourcing architecture
- **Command:** In general we can say command is a specific action. Mostly performed by some user. Example: placing an order in an ecommerce website.
- **Command Handler:** The main work of command handler is to check whether the given command is valid or not, also validate the other details if everything works fine then command handler will generate an event or group of event. Example: after we placed an order this will go to handler which will check for each details and the availability if everything works fine then it will create a event for successful order.
- **Event Store:** As the name explains itself, a place where logs of every event will store.
- **Event Bus:** It is also known as message broker which serves as mediator, to facilitate exchange of events between different components within the system.
- **Read Model:** It is maintained asynchronously by consuming events from event store and converting them into a format suitable for querying.
- **Projection:** Projection helps in updating read model and each projection project to relevant events. they use different strategies like building from scratch, caching etc for updation purpose.
- **Query Handler:** Read operations are handled by query. So, a program needed for execution and management of queries which is query handler.
- **User Interface:** It is responsible for capturing inputs from user and presenting required result.

## Advantages
- **Auditability:** Event Sourcing Architecture provides a reliable audit trail of all state changes, enabling forensic analysis and compliance with regulatory requirements.
- **Flexibility:** The ability to replay events allows systems to evolve over time without losing historical data or disrupting existing functionality.
- **Scalability:** By decoupling read and write operations and leveraging distributed event stores, Event Sourcing Architecture enables horizontal scalability.

## Basic Examples of event Sourcing
- **Git:** Everyone who is from technical domain knows about git. It also do event sourcing in the form of commits which help in versioning.
- **Multiplayer Games:** Multiplayer games also use event sourcig for better and fair play experience where if one player do saome action then at similar time other player timestamp will be looked by server and after comparing event action will be executed.

## Conclusion

## References
1. https://medium.com/design-microservices-architecture-with-patterns/event-sourcing-pattern-in-microservices-architectures-e72bf0fc9274
2. https://www.youtube.com/watch?v=rJHTK2TfZ1I
3. https://www.youtube.com/watch?v=AUj4M-st3ic&list=PLThyvG1mlMzkRKJnhzvxtSAbY8oxENLUQ
4. https://ibm-cloud-architecture.github.io/refarch-eda/patterns/event-sourcing/



